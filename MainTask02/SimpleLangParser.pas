unit SimpleLangParser;
 
interface
 
procedure Progr;
procedure Expr;
procedure Assign;
procedure StatementList;
procedure Statement; 
procedure Block;
procedure Cycle;
procedure &For;
procedure syntaxerror(message: string := '');
 
implementation
 
uses 
  SimpleLangLexer;
 
procedure Progr;
begin
  Block
end;
 
procedure Statement;
begin
  case LexKind of
Tok.&Begin: Block; 
Tok.Cycle: Cycle;
Tok.Id: Assign;
Tok.&FOR: &For;
else syntaxerror('�������� ��������');
  end;
end;
 
procedure Block;
begin
  NextLexem; // ������� begin
  StatementList;
  if LexKind=Tok.&End then
    NextLexem
  else syntaxerror('��������� end');    
end;

procedure &For;
begin
  NextLexem; // ������� for
  if LexKind = Tok.ID then
    NextLexem
  else syntaxerror('��������� id');
  if LexKind = Tok.ASSIGN then
    NextLexem
  else syntaxerror('��������� :=');
  Expr;
  if LexKind = Tok.&To then
    NextLexem
  else syntaxerror('��������� to');
  Expr;
  if LexKind = Tok.&Do then
    NextLexem
  else syntaxerror('��������� do');
  Statement;
end;
 
procedure Cycle;
begin
  NextLexem; // ������� cycle
  Expr;
  Statement;
end;
 
procedure Assign;
begin
  NextLexem; // ������� id
  if LexKind = Tok.ASSIGN then
    NextLexem
  else syntaxerror('��������� :=');  
  Expr;  
end;
 
procedure StatementList;
begin
  Statement;
  while LexKind=Tok.SEMICOLON do
  begin
    NextLexem;
    Statement;
  end
end;
 
procedure Expr; // ��������� - ��� id ��� num
begin
  if LexKind in [Tok.ID,Tok.INUM] then
    NextLexem 
  else syntaxerror('��������� ���������');  
end;
 
procedure syntaxerror(message: string);
begin
  var ss := System.IO.File.ReadLines(fname).Skip(LexRow-1).First(); // ������ row �����
  writeln('�������������� ������ � ������ ',LexRow,':');
  writeln(ss);
  writeln('^':LexCol-1);
  if message<>'' then 
    writeln(message);
  Done;  
  halt;
end;
 
end.