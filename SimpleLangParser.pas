unit SimpleLangParser;
 
interface
 
procedure Progr;
procedure Expr;
procedure Assign;
procedure StatementList;
procedure Statement; 
procedure Block;
procedure Cycle;
procedure syntaxerror(message: string := '');
 
implementation
 
uses 
  SimpleLangLexer;
 
procedure Progr;
begin
  Block
end;
 
procedure Statement;
begin
  case LexKind of
Tok.&Begin: Block; 
Tok.Cycle: Cycle;
Tok.Id: Assign;
else syntaxerror('�������� ��������');
  end;
end;
 
procedure Block;
begin
  NextLexem; // ������� begin
  StatementList;
  if LexKind=Tok.&End then
    NextLexem
  else syntaxerror('��������� end');    
end;
 
procedure Cycle;
begin
  NextLexem; // ������� cycle
  Expr;
  Statement;
end;
 
procedure Assign;
begin
  NextLexem; // ������� id
  if LexKind = Tok.ASSIGN then
    NextLexem
  else syntaxerror('��������� :=');  
  Expr;  
end;
 
procedure StatementList;
begin
  Statement;
  while LexKind=Tok.SEMICOLON do
  begin
    NextLexem;
    Statement;
  end
end;
 
procedure Expr; // ��������� - ��� id ��� num
begin
  if LexKind in [Tok.ID,Tok.INUM] then
    NextLexem 
  else syntaxerror('��������� ���������');  
end;
 
procedure syntaxerror(message: string);
begin
  var ss := System.IO.File.ReadLines(fname).Skip(LexRow-1).First(); // ������ row �����
  writeln('�������������� ������ � ������ ',LexRow,':');
  writeln(ss);
  writeln('^':LexCol-1);
  if message<>'' then 
    writeln(message);
  Done;  
  halt;
end;
 
end.